package MarsRoverKata;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.Assert.*;

public class ServiceDirectionTest {
    ServiceDirection serviceDirection;
    private Position x;
    private Position y;
    private final Direction direction = Direction.NORTH;

    @Before
    public void beforeServiceDirectionTest() {
        x = new Position(4, 50);
        y = new Position(5, 50);
        serviceDirection = new ServiceDirection(x, y, direction);
    }

    @Test
    public void newInstanceparm() {
        assertEquals(serviceDirection.getX(), x);
        assertEquals(serviceDirection.getY(), y);
    }

    @Test
    public void newInstanceDirection() {
        assertEquals(serviceDirection.getDirection(), direction);
    }

    //test moveForward
    @Test
    public void moveForwardNorth() {
        Position excepted = new Position(y.getPosition() + 1, y.getMaxPosition());
        serviceDirection.setDirection(Direction.NORTH);
        serviceDirection.moveForward();
        assertThat(serviceDirection.getY()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveForwardEast() {
        Position excepted = new Position(x.getPosition() + 1, x.getMaxPosition());
        serviceDirection.setDirection(Direction.EAST);
        serviceDirection.moveForward();
        assertThat(serviceDirection.getX()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveForwardWest() {
        Position excepted = new Position(x.getPosition() - 1, x.getMaxPosition());
        serviceDirection.setDirection(Direction.WEST);
        serviceDirection.moveForward();
        assertThat(serviceDirection.getX()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveForwardSouth() {
        Position excepted = new Position(y.getPosition() - 1, y.getMaxPosition());
        serviceDirection.setDirection(Direction.SOUTH);
        serviceDirection.moveForward();
        assertThat(serviceDirection.getY()).isEqualToComparingFieldByField(excepted);
    }

    //testmoveB
    @Test
    public void moveBackNorth() {
        Position excepted = new Position(y.getPosition() - 1, y.getMaxPosition());
        serviceDirection.setDirection(Direction.NORTH);
        serviceDirection.moveBackward();
        assertThat(serviceDirection.getY()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveBackEast() {
        Position excepted = new Position(x.getPosition() - 1, x.getMaxPosition());
        serviceDirection.setDirection(Direction.EAST);
        serviceDirection.moveBackward();
        assertThat(serviceDirection.getX()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveBackdWest() {
        Position excepted = new Position(x.getPosition() + 1, x.getMaxPosition());
        serviceDirection.setDirection(Direction.WEST);
        serviceDirection.moveBackward();
        assertThat(serviceDirection.getX()).isEqualToComparingFieldByField(excepted);
    }

    @Test
    public void moveBackSouth() {
        Position excepted = new Position(y.getPosition() + 1, y.getMaxPosition());
        serviceDirection.setDirection(Direction.SOUTH);
        serviceDirection.moveBackward();
        assertThat(serviceDirection.getY()).isEqualToComparingFieldByField(excepted);
    }

}