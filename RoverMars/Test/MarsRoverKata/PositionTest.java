package MarsRoverKata;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;
import static org.junit.Assert.*;

public class PositionTest {
    Position positionOb;
    private final int position = 4;
    private final int maxPosition = 10;

    @Before
    public void beforePositionTest() {
        positionOb = new Position(position, maxPosition);
    }

    @Test
    public void testNewInstanceParm() {
        assertThat(positionOb.getPosition()).isEqualTo(position);
        assertThat(positionOb.getMaxPosition()).isEqualTo(maxPosition);
    }

    @Test
    public void testGetBackPosition() {
        int excepted = positionOb.getPosition() - 1;
        assertThat(positionOb.getBackwardPostion()).isEqualTo(excepted);
    }

    @Test
    public void testBackPositionMaxPostion() {
        //should return getMaxPostion
        positionOb.setPosition(0);
        assertThat(positionOb.getBackwardPostion()).isEqualTo(positionOb.getMaxPosition());
    }
}