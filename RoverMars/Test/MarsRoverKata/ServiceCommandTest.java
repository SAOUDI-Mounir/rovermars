package MarsRoverKata;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.*;

public class ServiceCommandTest {
    private ServiceCommand serviceCommand;
    private final Direction direction = Direction.NORTH;
    private Position x;
    private Position y;
    private ServiceDirection commandDirection;

    @Before
    public void beforCommandTest() {
        x = new Position(0, 9);
        y = new Position(0, 9);
        commandDirection = new ServiceDirection(x, y, direction);
        serviceCommand = new ServiceCommand(commandDirection);
    }

    @Test
    public void receiveCommandL() throws Exception {
        serviceCommand.receiveCommand('L');
        assertThat(serviceCommand.getServiceDirection().getDirection()).isEqualTo(Direction.WEST);
    }

    @Test
    public void receiveCommandR() throws Exception {
        serviceCommand.receiveCommand('R');
        assertThat(serviceCommand.getServiceDirection().getDirection()).isEqualTo(Direction.EAST);
    }

    @Test
    public void receiveCommandX() throws Exception {
        serviceCommand.receiveCommand('X');
        assertThat(serviceCommand.getServiceDirection().getDirection()).isEqualTo(Direction.EAST);
    }


    @Test
    public void positionSouldReturnDirection() throws Exception {
        serviceCommand.reciveCommandString("RFFFFLFFFF");
        assertThat(serviceCommand.getOutPut()).isEqualTo("4 4 N");
    }
}
