package MarsRoverKata;

public enum Direction {

    NORTH(0, 'N'),
    EAST(1, 'E'),
    SOUTH(2, 'S'),
    WEST(3, 'W');
    private char dir;
    private int value;


    public char getDir() {
        return dir;
    }

    private Direction(int dValue, char directionVal) {

        value = dValue;
        dir = directionVal;

    }

    public int getValue() {
        return value;
    }

    public Direction getBDirection() {
        return values()[(this.getValue() + 2) % 4];
    }

}


