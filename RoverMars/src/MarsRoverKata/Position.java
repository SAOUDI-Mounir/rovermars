package MarsRoverKata;

public class Position {


    private int position;
    private int maxPosition;

    public Position(int positionValue, int maxPositionValue) {
        this.position = positionValue;
        this.maxPosition = maxPositionValue;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int positionVal) {
        this.position = positionVal;
    }


    public int getMaxPosition() {
        return maxPosition;
    }

    public void setMaxPosition(int maxPositionVal) {
        this.maxPosition = maxPositionVal;
    }


    public int getBackwardPostion() {
        if (getPosition() > 0)
            return getPosition() - 1;
        else return getMaxPosition();
    }

}
