package MarsRoverKata;

public class ServiceCommand {


    private ServiceDirection serviceDirection;

    public ServiceCommand(ServiceDirection serviceDirectionValue) {
        this.serviceDirection = serviceDirectionValue;
    }

    public ServiceDirection getServiceDirection() {
        return serviceDirection;
    }

    public void setServiceDirection(ServiceDirection value) {
        this.serviceDirection = value;
    }

    public boolean receiveCommand(char command) throws Exception {
        switch (Character.toUpperCase(command)) {
            case 'F':
                return getServiceDirection().moveForward();
            case 'B':
                return getServiceDirection().moveBackward();
            case 'L':
                getServiceDirection().changeDirectionLeft();
                return true;
            case 'R':
                getServiceDirection().changeDirectionRight();
                return true;
            default:
                throw new Exception("Command" + command + " is unknown");
        }
    }

    public void reciveCommandString(String commands) throws Exception {
        for (char command : commands.toCharArray()) {
            if (!receiveCommand(command)) {
                break;
            }
        }
    }

    public String getOutPut() {
        return serviceDirection.getX().getPosition() + " " + serviceDirection.getY().getPosition() + " " + serviceDirection.getDirection().getDir();

    }
}
