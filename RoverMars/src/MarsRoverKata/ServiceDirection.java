package MarsRoverKata;

public class ServiceDirection {

    private Position x;
    private Position y;


    private Direction direction;

    public ServiceDirection(Position xValue, Position yValue, Direction directionValue) {

        this.x = xValue;
        this.y = yValue;
        this.direction = directionValue;
    }

    public Direction getDirection() {
        return direction;
    }

    public void setDirection(Direction directionValue) {
        this.direction = directionValue;
    }

    public Position getX() {
        return x;
    }

    public void setX(Position xvalue) {
        this.x = xvalue;
    }

    public Position getY() {
        return y;
    }

    public void setY(Position yvalue) {
        this.y = yvalue;
    }

    protected boolean move(Direction directionValue) {
        int xPosition = x.getPosition();
        int yPosition = y.getPosition();
        switch (directionValue) {
            case NORTH:
                //yPosition = y.getForwardPostion();
                yPosition = (y.getPosition() + 1) % (y.getMaxPosition() + 1);
                break;
            case EAST:
                //xPosition = x.getForwardPostion();
                xPosition = (x.getPosition() + 1) % (x.getMaxPosition() + 1);
                break;
            case SOUTH:
                yPosition = y.getBackwardPostion();
                break;
            case WEST:
                xPosition = x.getBackwardPostion();
                break;
        }
        x.setPosition(xPosition);
        y.setPosition(yPosition);
        return true;
    }

    public boolean moveForward() {
        return move(direction);
    }

    public boolean moveBackward() {
        return move(direction.getBDirection());
    }

    public void changeDirection(Direction directionValue, int directionStep) {
        int directions = Direction.values().length;
        int ind = (directions + directionValue.getValue() + directionStep) % directions;
        direction = Direction.values()[ind];
    }

    public void changeDirectionLeft() {
        changeDirection(direction, -1);
    }

    public void changeDirectionRight() {
        changeDirection(direction, 1);
    }
}
